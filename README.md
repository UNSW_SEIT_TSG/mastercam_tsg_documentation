# Mastercam TSG Documentation

Configuration instructions for Mastercam and associated software for use with CNC machines in TSG Staff workshop.

## Configuring a new installation of Mastercam 2020
### Install Mastercam 2020
   * Run ``launcher.exe`` from ``\\ad.unsw.edu.au\oneunsw\CAN\SEIT\TSG\ACME Mechanical\Mastercam\mastercam_2020_installation_media``.
   * Configure details at the appropriate step.
### Update local user settings by copying template directory
   * from ``\\ad.unsw.edu.au\oneunsw\CAN\SEIT\TSG\ACME Mechanical\Mastercam\mastercam_tsg_documentation\resources\My Mastercam 2020``
   * to ``C:\Users\<unsw_identifier>\Documents\My Mastercam 2020``
### Install NPort Version 1.22 (Note later versions seem to be incompatible with the communications equipment in the workshop).
   * Installation file can be found here: ``\\ad.unsw.edu.au\oneunsw\CAN\SEIT\TSG\ACME Mechanical\Mastercam\mastercam_tsg_documentation\installation_media\Npadm_Setup_Ver1.22_Build_16082410.exe``
### Launch NPort and configure COM Ports
   * Once configured exit NPort.
### Overwrite CIMCO Machines folder
   * Navigate to "%AppData%\Roaming\CIMCO AS\CIMCOEdit8"
   * Replace or create the Machines Folder
### Moxa NPORT
It is possible this step may be redundant, but need to check with Jack.
   * Run moxa Nport
   * search for com ports
   * Import "Shop PC 1"
   * save
### Launch Mastercam 2020
### Testing steps
#### Prepare CNC machine to receive machining instructions
   * list prog
   * cursor on all
   * end
   * recv

### Configuration of communications with Licence Server
* mastercam_tsg_documentation/licence_stuff/nethasp.ini
* mastercam_tsg_documentation/licence_stuff/nhasp_dialog.png
* `CAN-DS-SEIT1011.adns.unsw.edu.au NetHasp License Server`
* `131.236.54.162`

### Test communications with workshop machines
#### Workshop machines and port mappings
| Machine | Com Port |   IP. Address | Port | Description |
|---------|----------|---------------|------|-------------|
|    TL-1 |      102 | 131.236.52.60 |    1 | Lathe |
|    VF-0 |      100 | 131.236.52.62 |    2 | Haas Vertical Machining Centre (older model) |
|    VF-3 |      103 | 131.236.52.61 |    1 | Haas Vertical Machining Centre (https://www.haascnc.com/machines/vertical-mills/vf-series/models/medium/vf-3.html) |
|    TM-1 |      101 | 131.236.52.62 |    1 | Mill |

Note the above table is a reflection of the specification defined in ``nport_com_mapping.ini``.


## Reconfiguring an existing installation
### Update Registry Keys for Mastercam 2019 and 2020 respectively
Using ``regedit.exe`` update the following keys:
* ``Computer\HKEY_CURRENT_USER\Software\CNC Software, Inc.\Mastercam 2019``
    * ``SharedDir``: ``"\\ad.unsw.edu.au\OneUNSW\CAN\SEIT\TSG\ACME Mechanical\Mastercam 2019"``
* ``Computer\HKEY_LOCAL_MACHINE\Software\CNC Software, Inc.\Mastercam 2019``
    * ``SharedDir``: ``"\\ad.unsw.edu.au\OneUNSW\CAN\SEIT\TSG\ACME Mechanical\Mastercam 2019"``
* ``Computer\HKEY_CURRENT_USER\Software\CNC Software, Inc.\Mastercam 2020``
    * ``SharedDir``: ``"\\ad.unsw.edu.au\OneUNSW\CAN\SEIT\TSG\ACME Mechanical\Mastercam 2020"``
* ``Computer\HKEY_LOCAL_MACHINE\Software\CNC Software, Inc.\Mastercam 2020``
    * ``SharedDir``: ``"\\ad.unsw.edu.au\OneUNSW\CAN\SEIT\TSG\ACME Mechanical\Mastercam 2020"``
### Update local user settings to match defaults
Navigate to ``C:\Users\<unsw_identifier>\Documents\`` and replace directories:
 * ``my mcam2019`` and
 * ``My Mastercam 2020``

with the directories stored at ``\\ad.unsw.edu.au\oneunsw\CAN\SEIT\TSG\ACME Mechanical\Mastercam\mastercam_tsg_documentation\resources``.

These directories store the user settings data and machine addresses.



## Contacts
For futher assistance contact either:
* Jack Pidgeon <j.pidgeon@adfa.edu.au>; or
* John Forbes <j.forbes@adfa.edu.au>.

## Mastercam 2020 to CNC Machine Test Log

## Test/Task Desriptions:
| Test | Description |
| --- | --- |
| 2020 | Installed and configured Mastercam 2020 with common shared directory: ```\\ad.unsw.edu.au\OneUNSW\CAN\SEIT\TSG\ACME . |Mechanical\Mastercam 2020```
| 2019 | Installed and configured Mastercam 2019 with common shared directory: ```\\ad.unsw.edu.au\OneUNSW\CAN\SEIT\TSG\ACME . |Mechanical\Mastercam 2019```
| Nprt | Installed NPort v1.22 and removed any other versions; imported working port configuration ```\\ad.unsw.edu. |au\OneUNSW\CAN\SEIT\TSG\ACME .Mechanical\Mastercam 2019\_INSTALLS\Machine ports\Working_2020_01_31.ini```
| TL-1 | Successful transmission of CNC instructions from mastercam via CIMCO and NPort to Plant TL-1. |
| VF-0 | Successful transmission of CNC instructions from mastercam via CIMCO and NPort to Plant VF-0. |
| VF-3 | Successful transmission of CNC instructions from mastercam via CIMCO and NPort to Plant VF-3. |
| TM-1 | Successful transmission of CNC instructions from mastercam via CIMCO and NPort to Plant TM-1. |

### 2020 Jan 31
On 2020 January 31st the desktop computers used by the mechanical workshop staff were configured by 'Greg Williams' of MasterCam Software Australia reviewed the software configuration.

|     Machine | User | 2019 | 2020 | Nprt | TL-1 | VF-0 | VF-3 | TM-1 | Notes |
|-------------|------|------|------|------|------|------|------|------|-------|
|        C855 | Shared Workshop Account | Pass | Pass | Pass | Pass | Pass | Pass | Pass |       |
|        C922 | Brendan Wallace | Pass | Pass | Pass | Pass | Pass | Busy | Pass | VF-3 was occupied with a scheduled job and was not available for testing.|
| CAN-DS-C896 | Shared Workshop Account | N/A  | Pass | Pass | Pass | Pass | Pass | Pass | Did not install 2019 as this was a freshly imaged windows 10 machine and 2020 was sufficient. |

#### Untested machines
These machines were not formally tested at the end of the configuration but were believed to have been properly configured by close of business.
|     Machine | User |
|-------------|------|
|        C865 | Jack Pidgeon |
|        C910 | Rick Whyte |
|       C1245 | James Bowman |
|       C880 | JJ Oosthuizen |

The machine used by Mark Dumbrell was not configured or tested.